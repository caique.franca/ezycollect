##### EC2 Variables #####
variable "region" {
  type = map
  default = {
    canada   = "ca-central-1"
    virginia = "us-east-1"
  }
}

variable "amis" {
  description = "Ubuntu ami"
  type = map
  default = {
    us-east-1 = "ami-042e8287309f5df03"
    us-east-1-amazon-linux = "ami-0533f2ba8a1995cf9"
    us-east-2 = "ami-089c6f2e3866f0f14"
    ca-central-1 = "ami-0aee2d0182c9054ac"
  }
}

variable "cdirs_acesso_remoto" {
    type = list
    default = [
        "191.35.25.0/24",
        "191.35.26.0/24"
    ]
}

variable "key_ssh_name" {
  type = string
  default = "id_bionic"
}

variable "ec2_name" {
  description = "EC2 name"
  type        = string
  default    = "ec2_EziCollect_bastion"
}

variable "sbn_name" {
  description = "Subnet name"
  type        = string
  default    = "sbn_EziCollect_bastion"
}
##### EC2 Variables #####

##### SQS Variables #####
variable "create" {
  description = "Whether to create SQS queue"
  type        = bool
  default     = true
}

variable "sqs_name" {
  description = "This is the human-readable name of the queue. If omitted, Terraform will assign a random name."
  type        = string
  default     = "sqs_EzyCollect"
}

variable "name_prefix" {
  description = "A unique name beginning with the specified prefix."
  type        = string
  default     = null
}

variable "visibility_timeout_seconds" {
  description = "The visibility timeout for the queue. An integer from 0 to 43200 (12 hours)"
  type        = number
  default     = 30
}

variable "message_retention_seconds" {
  description = "The number of seconds Amazon SQS retains a message. Integer representing seconds, from 60 (1 minute) to 1209600 (14 days)"
  type        = number
  default     = 345600
}

variable "max_message_size" {
  description = "The limit of how many bytes a message can contain before Amazon SQS rejects it. An integer from 1024 bytes (1 KiB) up to 262144 bytes (256 KiB)"
  type        = number
  default     = 262144
}

variable "delay_seconds" {
  description = "The time in seconds that the delivery of all messages in the queue will be delayed. An integer from 0 to 900 (15 minutes)"
  type        = number
  default     = 0
}

variable "receive_wait_time_seconds" {
  description = "The time for which a ReceiveMessage call will wait for a message to arrive (long polling) before returning. An integer from 0 to 20 (seconds)"
  type        = number
  default     = 0
}

variable "policy" {
  description = "The JSON policy for the SQS queue"
  type        = string
  default     = ""
}

variable "redrive_policy" {
  description = "The JSON policy to set up the Dead Letter Queue, see AWS docs. Note: when specifying maxReceiveCount, you must specify it as an integer (5), and not a string (\"5\")"
  type        = string
  default     = ""
}

variable "fifo_queue" {
  description = "Boolean designating a FIFO queue"
  type        = bool
  default     = false
}

variable "content_based_deduplication" {
  description = "Enables content-based deduplication for FIFO queues"
  type        = bool
  default     = false
}

variable "kms_master_key_id" {
  description = "The ID of an AWS-managed customer master key (CMK) for Amazon SQS or a custom CMK"
  type        = string
  default     = null
}

variable "kms_data_key_reuse_period_seconds" {
  description = "The length of time, in seconds, for which Amazon SQS can reuse a data key to encrypt or decrypt messages before calling AWS KMS again. An integer representing seconds, between 60 seconds (1 minute) and 86,400 seconds (24 hours)"
  type        = number
  default     = 300
}

variable "deduplication_scope" {
  description = "Specifies whether message deduplication occurs at the message group or queue level"
  type        = string
  default     = null
}

variable "fifo_throughput_limit" {
  description = "Specifies whether the FIFO queue throughput quota applies to the entire queue or per message group"
  type        = string
  default     = null
}
##### SQS Variables #####

##### S3 Variables #####
variable "lifecycle_rule" {
  description = "List of maps containing configuration of object lifecycle management."
  type = map
  default = {
    standard_ia = 30
    glacier     = 60
    expiration  = 90
    prefix_all  = "/"
  }
}