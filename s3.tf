resource "aws_s3_bucket" "s3_bucket" {
  bucket            = "s3-bucket-ezycollect"
  acl               = "public-read"

  versioning {
    enabled         = true
  }

  lifecycle_rule {
    prefix          = var.lifecycle_rule["prefix_all"]
    enabled         = true

    noncurrent_version_transition {
      days          = var.lifecycle_rule["standard_ia"]
      storage_class = "STANDARD_IA"
    }

    noncurrent_version_transition {
      days          = var.lifecycle_rule["glacier"]
      storage_class = "GLACIER"
    }

    noncurrent_version_expiration {
      days          = var.lifecycle_rule["expiration"]
    }
  } 

  tags = {
    "Name"          = "s3_bucket_EzyCollect"
    "Environment"   = "dev"
  }
}