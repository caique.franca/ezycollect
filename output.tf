output "sqs_queue_id" {
  description = "The URL for the created Amazon SQS queue"
  value = element(
    concat(
      aws_sqs_queue.queue.*.id,
      [""],
    ),
    0,
  )
}

output "sqs_queue_arn" {
  description = "The ARN of the SQS queue"
  value = element(
    concat(
      aws_sqs_queue.queue.*.arn,
      [""],
    ),
    0,
  )
}

output "sqs_queue_name" {
  description = "The name of the SQS queue"
  value = element(
    concat(
      data.aws_arn.queue.*.resource,
      [""],
    ),
    0,
  )
}

output "s3_bucket_id" {
    value = aws_s3_bucket.s3_bucket.id
}
output "s3_bucket_arn" {
    value = aws_s3_bucket.s3_bucket.arn
}
output "s3_bucket_domain_name" {
    value = aws_s3_bucket.s3_bucket.bucket_domain_name
}
output "s3_hosted_zone_id" {
    value = aws_s3_bucket.s3_bucket.hosted_zone_id
}
output "s3_bucket_region" {
    value = aws_s3_bucket.s3_bucket.region
}