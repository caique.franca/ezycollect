FROM ubuntu:18.04

RUN apt-get update
#RUN apt-get upgrade -y
RUN apt-get install software-properties-common -y
RUN apt-get install unzip -y
#RUN apt-get install linux-headers-$(uname -r) -y
RUN apt-get install apt-transport-https -y
RUN apt-get install ca-certificates -y
RUN apt-get install curl -y
RUN apt-get install gnupg -y
RUN apt-get install lsb-release -y
RUN apt-get install openssh-client -y
RUN apt-get install iputils-ping -y

RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
RUN curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
RUN apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
RUN apt-get update && apt-get install terraform -y

RUN curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
RUN unzip awscliv2.zip
RUN ./aws/install

RUN curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"
RUN install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

CMD ["bash"]