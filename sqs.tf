resource "aws_sqs_queue" "queue" {
  count = var.create ? 1 : 0
  
  name                        = var.sqs_name
  name_prefix                 = var.name_prefix

  delay_seconds               = var.delay_seconds
  max_message_size            = var.max_message_size
  message_retention_seconds   = var.message_retention_seconds
  receive_wait_time_seconds   = var.receive_wait_time_seconds

  fifo_queue                  = var.fifo_queue
  fifo_throughput_limit       = var.fifo_throughput_limit
  content_based_deduplication = var.content_based_deduplication
  deduplication_scope         = var.deduplication_scope

  policy                      = var.policy

  tags = {
    "Name"                      = "sqs-EziCollect"
    "Environment"               = "dev"
  }

}

data "aws_arn" "queue" {
  count = var.create ? 1 : 0
  arn   = aws_sqs_queue.queue[0].arn
}